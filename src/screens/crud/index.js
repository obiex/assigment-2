import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class index extends Component {
    constructor() {
        super();
        this.state = {
            data: [
                { id: 1, name: 'mahrus', addres: 'tanggerang' },
                { id: 2, name: 'Farid', addres: 'Lampung' },
                { id: 3, name: 'subhan', addres: 'jakarta' },
                { id: 4, name: 'pur', addres: 'purwokerto' },
                { id: 5, name: 'joni', addres: 'Tangerang' },
                { id: 6, name: 'dimitri', addres: 'rusia' },
            ],
            name: '',
            addres: '',
        }
    }

    render() {
        return (
            <View >
                {this.state.data.map((value, i) => {
                    return <View style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                            <Text style={{ fontWeight: 'bold' }}> {value.id}</Text>
                            <Text style={{ fontWeight: 'bold' }}> {value.name}</Text>
                            <Text style={{ fontWeight: 'bold' }}> {value.addres}</Text>
                        </View>
                    </View>
                })}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    card: {
        padding: 10,
        borderRadius: 20,
        borderwidth: 1,
        margin: 5,

    },
    TextInput: {
        alignItems: 'center',
        marginTop: 15
    }
})
