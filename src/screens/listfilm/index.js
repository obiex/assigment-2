import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator, ImageBackground, ScrollView} from 'react-native';


export default class App extends Component {
    constructor(params) {
        super(params);
        this.state = {
            data: [],
    
        };
    }


    componentDidMount() {
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4')
            .then(result => result.json())
            .then(result => this.setState({ data: result.Search }));
    }

    render() {
        const { data } = this.state;
        console.log(data);
        return (
          
                <View >
                    <View style={{backgroundColor: '#32DE8A', alignItems: 'center', fontSize: 50, }}> 
                
                <Text style={{fontSize: 50}}>Daftar Film</Text>
                </View>
                    <ScrollView>
                {data && data.map((value, Index) => {
                    if (value.Year == 2019
                    ) 
                    return (
                        
                        <View style={{backgroundColor: '#29a391'}}>
                             <ScrollView style={styles.ScrollView}>
                           <Image source={{ uri: `${value.Poster}` }} style={styles.Image}
                          />
                          <View style={styles.textItemLogin} >
                            <Text style={{fontSize: 20}}>{value.Title} </Text>
                            <Text style={{fontSize: 20}}>{value.Type} </Text>
                            <Text style={{fontSize: 20}}>{value.Movie}</Text>
                            <Text style={{fontSize: 20}}>{value.Year}</Text>
                            </View>
                            </ScrollView>
                    
                        </View>
                    );
           })}
            </ScrollView>
            </View>
           
        );
    }
}




const styles = StyleSheet.create({
    viewList: {
        borderWidth: 10,
        borderColor: '#DDD',
        alignSelf: 'center',
        fontSize: 50,
        
    },
    Image: {
        width: 200,
        height: 300,
        borderRadius: 40,
        alignSelf: 'baseline', 
        marginVertical: 8,
        marginHorizontal: 16,      
    },

    ScrollView: {
     
        backgroundColor: '#627C85',
        marginHorizontal: 10,     
        borderBottomWidth : 1  
    },
    textItemLogin: {
        backgroundColor: '#29A391',
        padding: 20,
         marginVertical: 25,
         marginHorizontal: 25,
        textAlign: 'center',
        marginVertical: 8,
        alignSelf:'stretch',
        fontWeight: 'bold',
        color: 'white',
        fontFamily:'arial',
        borderRadius: 40,
        fontSize:100,   
        borderBottomWidth: 5,  
        },

    Headers: {
   
        fontSize: 100,
        fontWeight: "bold",
        },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    }
})