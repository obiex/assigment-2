import React, { Component } from 'react';
import {
  Button,
  Alert,
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  ImageBackground,
} from 'react-native';
import { Rosenke } from '../../assets/media';
import CButton from '../../component/atom/CButton';
import Ctext from '../../component/atom/Ctext';
import statment from '../../screens/statment';
import lifecycle from '../../screens/lifecycle';


export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: "",
    }

  }

  validate_field = () => {
    const { username, password } = this.state
    if (username == "") {
      alert("Email jangan dikosongi")
      return false
    } else if (password == "") {
      alert("Password jangan dikosongi")
      return false
    }
    return true
  }

  making_api_call = () => {
    if (this.validate_field()) {
      this.props.navigation.navigate('listfilm')

    }


  }
  componentDidMount() {
    fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4')
      .then(result => result.json())
      .then(result => this.setState({ data: result.Search }));
  }

  render() {
    return (
      <View style={Styles.ViewContainer}>
        <Image
          source={{
            uri: 'https://img.icons8.com/ios/50/000000/facebook-new.png',
          }}
        />
        <View style={Styles.viewWrapper}>
          <View style={Styles.textAlign} />
          <Image
            source={{ uri: 'asset:/rosenka.jpg' }}
            style={{ width: 100, height: 40 }}
          />
          <Image
            source={{
              uri: 'https://reactjs.org/logo-og.png',
            }}
            style={Styles.imageLogin}
          />
          <Text style={Styles.TextTitle}>SILAHKAN LOGIN</Text>
          <View>
            <TextInput placeholder="Masukan Email" style={Styles.textInput}
              onChangeText={(value) => this.setState({ username: value })}
              value={this.state.username}
            />
            <TextInput
              placeholder="Masukan Password" style={Styles.textInput}
              onChangeText={(value) => this.setState({ password: value })}
              value={this.state.password}
              secureTextEntry
            />
          </View>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('statment') }} style={Styles.ViewButton}>
            <Text style={Styles.TextLogin}>LOGIN</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('signup') }}>
            <Text style={{ color: 'white', marginTop: 5, alignSelf: 'center' }}>Daftar Sekarang</Text>
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 20 }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/color/48/000000/facebook.png',
              }}
              style={{ borderBottomWidth: 50, alignSelf: 'center', height: 50, width: 50, flexDirection: 'row-reverse' }}
            />
            <Image
              source={{
                uri: 'https://img.icons8.com/fluency/48/000000/instagram-new.png',
              }}
              style={{ borderBottomWidth: 5, alignSelf: 'auto', height: 50, width: 50, flexDirection: 'row' }}
            />
            <Image
              source={{
                uri: 'https://img.icons8.com/material-outlined/24/000000/whatsapp--v1.png',
              }}
              style={{ borderBottomWidth: 5, alignSelf: 'auto', height: 50, width: 50, flexDirection: 'row' }}
            />
            <View>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('axios') }}>
                <Text style={{ color: 'purple', marginTop: 5, alignSelf: 'center' }}>axios </Text>
              </TouchableOpacity>
            </View>
            <View>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('crud') }}>
                <Text style={{ color: 'purple', marginTop: 5, alignSelf: 'center' }}>crud </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('lifecycle') }}>
                <Text style={{ color: 'purple', marginTop: 5, alignSelf: 'center' }}>lifecycle </Text>
              </TouchableOpacity>
              <CButton onpress={() => { console.log('Tombol') }} style={{ backgroudcolor: 'red', }} title='Tombol' />
              <Ctext style={Styles.main}>Ini Tombol</Ctext>
            </View>
          </View>

        </View>


      </View>
    );
  }
}

const Styles = StyleSheet.create({
  ViewContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#cfe2f3',


  },
  textInput: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  TextTitle: {
    padding: 20,
    marginVertical: 10,
    marginHorizontal: 10,
    justifyContent: 'center',
    textAlign: 'center',
    marginVertical: 20,
    fontSize: 30,
    fontWeight: "bold",
  },
  TextLogin: {
    backgroundColor: '#F194FF',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    textAlign: 'center',
    marginVertical: 8,
    fontWeight: 'bold',
    color: 'yellow',
    fontSize: 20,

  },
  imageLogin: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  image: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',

  },
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row-reverse'
  },
    main: {
      backgroundColor: 'red',
      margin:10,
      
  },
});
