import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../screens/Home'
import SignUp from '../screens/signup'
import listfilm from '../screens/listfilm';
import Statment from'../screens/statment';
import crud from'../screens/crud';
import lifecycle from'../screens/lifecycle';
import splash from'../screens/splash';
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='splash'>
        <Stack.Screen options={{headerShown: false}} name='Home' component={Home} />
        <Stack.Screen options={{headerShown: false}} name='signup' component={SignUp} />
        <Stack.Screen name='listfilm' component={listfilm} />
        <Stack.Screen name='statment' component={Statment} />
        <Stack.Screen name='crud' component={crud} />
        <Stack.Screen name='lifecycle' component={lifecycle} />
        <Stack.Screen options={{headerShown: false}} name='splash' component={splash} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;