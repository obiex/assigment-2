import React, {Component} from "react";
import { Text, StyleSheet, view, TouchableOpacity } from "react-native";

export default class CButton extends Component {

    render() {
       const {title} = this.props
        return(
            <TouchableOpacity style={Styles.main} {...this.props}>
                <Text> {this.props.title }</Text>
            </TouchableOpacity>
        );
    }
}
const Styles = StyleSheet.create({
    
    main: {
      backgroundColor: 'red',
      margin:10,
      
    }
    })  
